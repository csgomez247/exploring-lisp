(defun double-recursive (y)

	(cond

		((not (listp y))
			nil
		);not (listp y)

		((null y)
			nil
		); null l

		(t
			(append

				(append
					(list 
						(car y)
					);list
					(list 
						(car y)
					);list
				);append

				(double-recursive 
					(cdr y)
				);double-recursive

			);cons
		);t

	);cond

); double-recursive

(defun double-iterative (y)

	(cond
		((not (listp y))
			nil
		); not (listp y)

		(t
			(do 
				;variables used
				(
					(currList y 
						(cdr currList)
					);currList

					(result nil
						(append
							result
							(append
								(list
									(car currList)
								);list
								(list
									(car currList)
								);list
							);append car currList & car currList
						);append result & doubler
					);result
				);variables used

				;termination condition
				((null currList) result)
			);do
		); t
	);cond

);double-iterative

(defun double-mapcar (y)
	(cond
		((not (listp y))
			nil
		);not (listp y)

		(t
			(apply
				'append
				(mapcar 
					'doubler
					y
				);mapcar
			);apply
		)
	);cond
);double-mapcar

(defun doubler (y)	
	(append
		(list y)
		(list y)
	);append
);double 

(defun rdouble (y)
	(cond

		((not (listp y))
			nil
		);y is not a list

		((null y)
			nil
		);y is an empty list

		(t
			(append
				(cond
					((not (listp (car y)))
						(doubler (car y))
					); if (car y) is not a list

					(t
						(list
							(rdouble (car y))
						);list
					);t
				);cond

				(rdouble
					(cdr y)
				);rdoube
			);append

		);else, if car y is not an atom
	);cond
);rdouble

(defun my-mapcar (fun l1 l2)

	(cond
		((null l1)
			nil
		);end of l1

		((null l2)
			nil
		);end of l2

		(t
			(append
				(list
					(funcall
						fun 
						(car l1)
						(car l2)
					);funcall
				);list

				
				(my-mapcar
						fun
						(cdr l1)
						(cdr l2)
				);my-mapcar
				
			);append
		);t
	);cond

);my-mapcar

(defun rewrite (x)
	(cond
		((null x)
			nil
		);if x is empty list

		((not (listp x))
			x
		);if x is not a list

		(t
			(append
				(cond
					((listp (car x))
						(list
							(rewrite
								(car x)
							);rewrite
						);list
					);(car x) is a list

					((equal (car x) 'if)
						(append
							(list
								'cond
							);list

							(list
								(list
									(rewrite
										(cadr x)
									);rewrite
									
									(rewrite
										(caddr x)
									);rewrite
								);
							);list

							(cond
								((null (cadddr x))
									nil
								);(cadddr x) is empty

								((not (equal (cadddr x ) 'if))
									(list
										(append
											(list
												't
											);list

											(list
												(rewrite
													(cadddr x)
												);rewrite
											);list
										);append
									);list
								);(cadddr x) not an if

								(t
									(list
										(rewrite
											(cadddr x)
										);rewrite
									);list
								);(cadddr x) is an if
							);cond
						);append
					);(car x) is an if

					(t
						(list
							(car x)
						);list
					);(car x) is not an if
				);cond

				(cond
					((equal (car x) 'if)
						(cond
							((not (equal (cadddr x) 'if))
								(rewrite
									(car (cddddr x))
								);rewrite
							);(cadddr x) is an else

							(t
								(rewrite
									(cadddr x)
								);rewrite
							);(cadddr x) is an if
						);cond
					);(car x) is if

					(t
						(rewrite
							(cdr x)
						);rewrite
					);(car x) is not if
				);cond
			);append
		);t
	);cond
);rewrite

(defun check (x)
	(append
		(list
			(cond
				((equal (eval x) (eval (rewrite x)))
					t
				); list 2 and 3 are equal
				(t
					nil
				);
			);cond
		);list

		(list
			(eval x)
		);list

		(list
			(eval
				(rewrite
					x
				);rewrite
			);eval
		);list
	);append
);check